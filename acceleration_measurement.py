import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_iphone.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
data = {sensor_settings_dict["ID"]: {"acceleration_x":[], "acceleration_y":[], "acceleration_z":[],"timestamp":[] }}
#

start = time.time() # startzeit wird festgelegt
while (time.time()-start <= measure_duration_in_s ): # Messdaten werden für Messdauer in Listen gespeichert
    messung= accelerometer.acceleration
    data[sensor_settings_dict["ID"]]["acceleration_x"].append(messung[0])
    data[sensor_settings_dict["ID"]]["acceleration_y"].append(messung[1])
    data[sensor_settings_dict["ID"]]["acceleration_z"].append(messung[2])
    data[sensor_settings_dict["ID"]]["timestamp"].append(time.time()-start)
    print("-") # zeigt an das Messung läuft
    time.sleep(0.001) # Messung pausiert für 0.001 sek

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start

f= h5py.File(path_h5_file,"r+") # HDF5 öffnen
grp_raw = f.create_group("RawData") # Gruppen erstellen
grp_sensor= grp_raw.create_group(sensor_settings_dict["ID"])
for key in ["acceleration_x","acceleration_y","acceleration_z","timestamp"]:
    grp_sensor.create_dataset(key, data = data[sensor_settings_dict["ID"]][key])
f.close() #HDF5 schließen

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
