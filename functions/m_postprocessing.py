"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z:np.ndarray ) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.
       
    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    acc = np.sqrt(np.power(x,2)+np.power(y,2)+np.power(z,2)) #gesamtbeschleunigung wird durch vektorfunktionen berechnet
    #beschleunigung wird zurück gegeben
    return acc

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    time_interp = np.arange(0,20,20/len(time)) #Zeit wird in gleiche schritte aufgeteilt
    new_acc = np.interp(time_interp,time,data) #interpolation mit np.interp
    
    return new_acc


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    x = x-np.mean(x) #Verschiebung der Daten 
    time_step = 20/len(time) #Schrittweite der Zeitabstände 
    amplitude = np.abs(np.fft.fft(x)) #amplitude mit fft berechnen
    freq = np.fft.fftfreq(len(x),time_step) # Frequeny mit np.fftfreq berechnen 
    # Amplitude und Frequenz als Tupel zurückgeben
    return (amplitude,freq)